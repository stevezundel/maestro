/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function(req, res){
        return res.view('Main/index');
    },
    test: function(req, res) {
        return res.view('Main/test');
    }
};

