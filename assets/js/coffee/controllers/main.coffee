window.Main or={}
window.Main.PieChart = class PieChart
  constructor: () ->
    @timeSpent = ($scope) ->
      $scope.labels = [">5 HRS", "1-3 HRS", "<1 HR",  "3-5 HRS", ];
      $scope.data = [10, 53.3, 16.7, 20 ];
      $scope.colors = ["#CCE5FF", "#CCDDFF", "#CCCCFF", "#CCEBFF"]
chart = new PieChart()
new window.Main.modules().maestro.controller('PieChart', chart.timeSpent)